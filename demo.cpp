//
// Created by cai on 2022/2/13.
//

#include <iostream>
#include "DbNet.h"
#include "Crnn.h"

using namespace cv;
using namespace std;

int main(int argc, char** argv){

    const char *db_model_path = argv[1];                 // 检测模型路径
    const char *crnn_model_path = argv[2];              // 识别模型路径
    const char *key_path = argv[3];                     // keys 路径
    const char *img_path = argv[4];                    // 测试图片路径

    DBNet dbNet;
    CRNN crnn;

    int retDbNet = dbNet.initModel(db_model_path);                                // 模型初始化
    int retCrnn = crnn.loadModel_init(crnn_model_path,key_path);

    if (retDbNet < 0 || retCrnn < 0){
        printf("load model fail!");
    }

    Mat image = imread(img_path);
    if (image.empty()) {
        cout << "Error: Could not load image" << endl;
        return -1;
    }
    //记录起始时间
    double time0 = static_cast<double>(getTickCount());

    vector<ImgBox> crop_img;
    crop_img = dbNet.getTextImages(image);                                       // 检测时间

    //计算运行时间并输出
    time0 = ((double) getTickCount() - time0) / getTickFrequency();                // 结束时间-开始时间，并化为秒单位
    cout << "\t检测运行时间为： " << time0 << "秒" << endl;                           // 输出运行时间

    //记录起始时间
    double time1 = static_cast<double>(getTickCount());
    vector<StringBox> result;
    result = crnn.inference(crop_img);                                         // 识别时间

    //计算运行时间并输出
    time1 = ((double) getTickCount() - time1) / getTickFrequency();            // 结束时间-开始时间，并化为秒单位
    cout << "\t识别运行时间为： " << time1 << "秒" << endl;                       // 输出运行时间
    cout << "\t总运行时间为： " << time0 +time1 << "秒" <<endl;

    for (auto &txt : result) {                                                // 输出识别结果
        cout << txt.txt << "\n" << endl;
        drawTextBox(image,txt.txtPoint,1);                        // 画框
    }
    cv::imwrite("img.jpg",image);

    return 0;
}